import 'package:elementeight/general.dart';
import 'package:elementeight/view/space_detail.dart';
import 'package:flutter/material.dart';

class SearchResults extends StatefulWidget {
  @override
  _SearchResultsState createState() => _SearchResultsState();
}

class _SearchResultsState extends State<SearchResults> with SingleTickerProviderStateMixin{
  List<Map<String, dynamic>> spaceTypes = [
    {'id' : 0,'type' : 'Space Type'}, {'id' : 1, 'type': 'Hotel'}, {'id' : 2, 'type': 'Classic'},
  ];
  int selectedType = 1;
  String _selectedDate = formatDateFromDate(DateTime.now(), 'dd MMM');
  TabController _tabController;
  TextEditingController locationController = TextEditingController();
  TextEditingController _numberOfPeople = TextEditingController();
  @override
  void initState() {
    _tabController = new TabController(length: 4, vsync: this);
    _numberOfPeople.text = '200';
    locationController.text = 'Jumeirah, Dubai, United Arab Emirates';
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(100),
        child: Container(
          color: Color(0xFF30318A),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AppBar(
                automaticallyImplyLeading: false,
                elevation: 0,
                backgroundColor: Color(0xFF30318A),
                title: Text('Search Results'),
                centerTitle: true,
                actions: [
                  Icon(Icons.person_outline)
                ],
                leading: Icon(Icons.dehaze),
              ),
            ],
          ),
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  padding: const EdgeInsets.all(30.0),
                  color: Colors.white.withOpacity(0.6),
                  child: Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10)
                        ),
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: ImageIcon(AssetImage('assets/images/pin.png'),color: Color(0xFF4C97A1),),
                            ),
                            Expanded(
                              child: TextField(
                                style: TextStyle(
                                    color: Color(0xFF7D7575)
                                ),
                                controller: locationController,
                                decoration: InputDecoration(
                                    hintText: 'Enter Your Location',
                                    border: InputBorder.none
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              padding: EdgeInsets.symmetric(vertical: 10),
                              margin: EdgeInsets.only(right: 5),
                              child: InkWell(
                                onTap: () => _selectDate(),
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(Icons.calendar_today, color: Color(0xFF4C97A1), size: 15,),
                                    ),
                                    Text(
                                      '$_selectedDate',
                                      style: TextStyle(
                                          color: Color(0xFF7D7575),
                                          fontSize: 16
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)
                              ),
                              margin: EdgeInsets.only(left: 5),
                              child: Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: ImageIcon(AssetImage('assets/images/type.png'),color: Color(0xFF4C97A1),),
                                  ),
                                  DropdownButton(
                                    underline: Container(),
                                    iconEnabledColor: Color(0xFF4C97A1),
                                    icon: Icon(Icons.keyboard_arrow_down),
                                    items: spaceTypes.map((e) {
                                      return DropdownMenuItem(
                                        value: e['id'],
                                        child: Text(
                                          e['type'],
                                          style: TextStyle(
                                              color: e['id'] == 0 ? Color(0xFF7D7575) : null
                                          ),
                                        ),
                                      );
                                    }).toList(),
                                    value: selectedType,
                                    onChanged: (value){
                                      setState(() {
                                        selectedType = value;
                                      });
                                    },
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10)
                        ),
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(Icons.person_outline,color: Color(0xFF4C97A1),),
                            ),
                            Expanded(
                              child: TextField(
                                style: TextStyle(
                                    color: Color(0xFF7D7575)
                                ),
                                controller: _numberOfPeople,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    hintText: 'Number Of People',
                                    border: InputBorder.none
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: InkWell(
                                child: Icon(Icons.keyboard_arrow_up,color: Color(0xFF4C97A1),),
                                onTap: (){
                                  if(_numberOfPeople.text != ''){
                                    _numberOfPeople.text = '${int.parse(_numberOfPeople.text) + 1}';
                                  }else{
                                    _numberOfPeople.text = '1';
                                  }
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: InkWell(
                                child: Icon(Icons.keyboard_arrow_down,color: Color(0xFF4C97A1),),
                                onTap: (){
                                  if(_numberOfPeople.text != '' && _numberOfPeople.text != '0'){
                                    _numberOfPeople.text = '${int.parse(_numberOfPeople.text) - 1}';
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                ListView.builder(
                  itemCount: 5,
                  shrinkWrap: true,
                  physics: PageScrollPhysics(),
                  itemBuilder: (BuildContext context, int index){
                    return InkWell(
                      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => SpaceDetail())),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white
                        ),
                        width: double.infinity,
                        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Stack(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), topLeft: Radius.circular(10)),
                                  child: Image.asset(
                                    'assets/images/ritz.jpg',
                                    fit: BoxFit.fill,
                                    width: MediaQuery.of(context).size.width/3,
                                    height: MediaQuery.of(context).size.width/3,
                                  ),
                                ),
                                Positioned(
                                  top: 5,
                                  left: 5,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.white,
                                    radius: 10,
                                    child: Icon(Icons.favorite_border, size: 15, color: Color(0xFFF77E0B),),
                                  ),
                                )
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      ImageIcon(AssetImage('assets/images/pin.png'),color: Color(0xFFF77E0B), size: 12,),
                                      Text(
                                        'JBR Jumeirah, Dubai',
                                        style: TextStyle(
                                            color: Color(0xFF919191)
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 10,),
                                  Text(
                                    'The Ritz-Carlton',
                                    style: TextStyle(
                                        color: Color(0xFF000000),
                                        fontSize: 17,
                                        fontWeight: FontWeight.w500
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 10, 5, 0),
                                        child: Icon(Icons.star, color: Color(0xFFF16F57), size: 15,),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 10, 5, 0),
                                        child: Icon(Icons.star, color: Color(0xFFF16F57), size: 15,),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 10, 5, 0),
                                        child: Icon(Icons.star, color: Color(0xFFF16F57), size: 15,),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 10, 5, 0),
                                        child: Icon(Icons.star, color: Color(0xFFF16F57), size: 15,),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 10, 5, 0),
                                        child: Icon(Icons.star, color: Color(0xFFC8C6C3), size: 15,),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 10,),
                                  Text(
                                    'AED 300/-',
                                    style: TextStyle(
                                        fontSize: 17,
                                        color: Color(0xFF30318A)
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                )
              ],
            ),
          ),
          Positioned(
            bottom: 20,
            right: 0,
            child: Container(
              width: 100,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color(0xFF424e99),
                      Color(0xFF6c94bd),
                    ],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                  ),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    bottomLeft: Radius.circular(30),
                  )
              ),
              padding: EdgeInsets.all(10),
              child: Row(
                children: [
                  ImageIcon(AssetImage('assets/images/filter.png'), color: Colors.white, size: 15,),
                  SizedBox(width: 10,),
                  Text(
                    'Filter',
                    style: TextStyle(
                        color: Colors.white
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        color: Colors.white,
        child: TabBar(
          controller: _tabController,
          indicatorColor: Color(0xFFF77E0B),
          labelColor: Color(0xFFF77E0B),
          unselectedLabelColor: Color(0xFF7D7575),
          tabs: [
            Tab(
              icon: ImageIcon(AssetImage('assets/images/space.png')),
              text: 'Space',
            ),
            Tab(
              icon: ImageIcon(AssetImage('assets/images/marquee.png')),
              text: 'Marquee',
            ),
            Tab(
              icon: ImageIcon(AssetImage('assets/images/car.png'),),
              text: 'Limousine',
            ),
            Tab(
              icon: ImageIcon(AssetImage('assets/images/buffet.png')),
              text: 'Catering',
            ),
          ],
        ),
      ),
    );
  }

  DateTime selectedDate = DateTime.now();
  Future<void> _selectDate() async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        _selectedDate = formatDateFromDate(picked, 'dd MMM');
      });
  }
}
