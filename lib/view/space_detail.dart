import 'package:flutter/material.dart';

class SpaceDetail extends StatefulWidget {
  @override
  _SpaceDetailState createState() => _SpaceDetailState();
}

class _SpaceDetailState extends State<SpaceDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled){
          return [
            SliverAppBar(
              title: Text('Space Detail'),
              centerTitle: true,
              leading: Icon(Icons.dehaze),
              expandedHeight: 200,
              flexibleSpace: Stack(
                children: [
                  Positioned.fill(
                    child: Image(
                      image: AssetImage('assets/images/ritz.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned.fill(
                    child: Container(
                      color: Colors.black.withOpacity(0.4),
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'The Ritz-Carlton',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 24,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              ImageIcon(AssetImage('assets/images/pin.png'),color: Colors.white, size: 12,),
                              Text(
                                'JBR Jumeirah, Dubai',
                                style: TextStyle(
                                    color: Colors.white
                                ),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 10, 5, 0),
                                child: Icon(Icons.star, color: Color(0xFFF16F57), size: 20,),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 10, 5, 0),
                                child: Icon(Icons.star, color: Color(0xFFF16F57), size: 20,),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 10, 5, 0),
                                child: Icon(Icons.star, color: Color(0xFFF16F57), size: 20,),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 10, 5, 0),
                                child: Icon(Icons.star, color: Color(0xFFF16F57), size: 20,),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 10, 5, 0),
                                child: Icon(Icons.star, color: Color(0xFFC8C6C3), size: 20,),
                              ),
                              Text(
                                '(8.6)',
                                style: TextStyle(
                                  color: Colors.white
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 20,
                    right: 0,
                    child: Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color(0xFF424e99),
                              Color(0xFF6c94bd),
                            ],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          ),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            bottomLeft: Radius.circular(30),
                          )
                      ),
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'AED 300/-',
                        style: TextStyle(
                          fontSize: 17,
                          color: Colors.white
                        ),
                      )
                    ),
                  ),
                ],
              ),
            )
          ];
        },
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            physics: PageScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  'ABOUT',
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Text(
                    'Pellentesque tristique sed arcu a euismod. Ut eget \mi viverra, sollicitudin metus nec, mattis elit donec. Nullam sed volutpat nisl. Donec id nisi nec quam at lacinia porta. Curabitur pharetra iaculis commodo.',
                    style: TextStyle(
                      fontSize: 15
                    ),
                  ),
                ),
                Text(
                  'Read More',
                  style: TextStyle(
                    color: Color(0xFF30318A),
                    fontSize: 17,
                    fontWeight: FontWeight.w500
                  ),
                  textAlign: TextAlign.right,
                ),
                SizedBox(height: 20,),
                Container(
                  padding: const EdgeInsets.all(20.0),
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        'PHOTOS',
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 20,),
                      Wrap(
                        spacing: 10,
                        runSpacing: 10,
                        alignment: WrapAlignment.center,
                        children: List.generate(6, (index) {
                          return ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Image(
                              image: AssetImage('assets/images/ritz.jpg'),
                              height: 100, width: MediaQuery.of(context).size.width/4,
                              fit: BoxFit.cover,
                            ),
                          );
                        }),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Container(
                  padding: const EdgeInsets.all(20.0),
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        'REVIEWS',
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 20,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Row(
                            children: [
                              CircleAvatar(
                                backgroundColor: Colors.blue,
                                child: Icon(Icons.person),
                              ),
                              SizedBox(width: 10,),
                              Text(
                                'Bartholomew Shoe',
                                style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500
                                ),
                              ),
                              Spacer(),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
                                child: Icon(Icons.star, color: Color(0xFFF16F57), size: 15,),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
                                child: Icon(Icons.star, color: Color(0xFFF16F57), size: 15,),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
                                child: Icon(Icons.star, color: Color(0xFFF16F57), size: 15,),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
                                child: Icon(Icons.star, color: Color(0xFFF16F57), size: 15,),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
                                child: Icon(Icons.star, color: Color(0xFFC8C6C3), size: 15,),
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: Color(0xFFF16F57),
                                  borderRadius: BorderRadius.circular(10)
                                ),
                                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                                child: Text(
                                  '5',
                                  style: TextStyle(
                                    color: Colors.white
                                  ),
                                ),
                              )
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 20),
                            child: Text(
                              'Aenean a eros elit. Etiam aliquet at dolor eu tortor hendrerit. Praesent a pretium diam, a lacinia nunc. Praesent pellentesque justo eget dignissim.',
                              style: TextStyle(
                                color: Color(0xFF5D5D5D),
                                fontSize: 15
                              ),
                            ),
                          ),
                          Text(
                            'Show More',
                            style: TextStyle(
                                color: Color(0xFF30318A),
                                fontSize: 17,
                                fontWeight: FontWeight.w500
                            ),
                            textAlign: TextAlign.right,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                RaisedButton(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Text(
                    'Book Now',
                    style: TextStyle(
                        color: Colors.white
                    ),
                  ),
                  color: Color(0xFF30318A),
                  onPressed: () {},
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
