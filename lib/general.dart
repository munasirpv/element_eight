import 'package:intl/intl.dart';
String formatDateFromDate(DateTime date, String outputFormat) {
  var outFormatter = new DateFormat(outputFormat);
  return outFormatter.format(date);
}